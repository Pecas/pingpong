# PingPong

## Introduction

Frontend de aplicación de ping pong, basada en material design.

## Installation

>Instalar Node-Npm
> $  install node
> Instalar Ionic de forma global
> $  npm install -g ionic
>Descargar proyecto
> <a href="https://bitbucket.org/Pecas/pingpong"> bitbucket</a><br>
>Ir al directorio
> $  cd pingpong-repo
>Correr aplicación
> $  ionic server


## Visualización XCODE

><b>instalar cordova ios</b><br>
> $ ionic cordova platform add ios
> entrar a carpeta plataforma y correr archivo xcode.

