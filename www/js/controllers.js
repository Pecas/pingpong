angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {
  $scope.winners = [
    { id: '0'  ,
      foto:'img/octavio.jpg',
      numero:'1',
      porcentaje:'100%',
      nombre:'Octavio Jimenez',
      border: 'border1'
      },
    { 
      id: '1'  ,
      foto:'img/david.jpg',
      numero:'2',
      porcentaje:'80%',
      nombre:'David Jimenez',
      border: 'border2'
  },
  { 
    id: '2'  ,
    foto:'img/miguel.jpg',
    numero:'3',
    porcentaje:'50%',
    nombre:'Miguel Novoa',
    border: 'border3'
  },
  { 
    id: '3'  ,
    foto:'img/octavio.jpg',
    numero:'4',
    porcentaje:'20%',
    nombre:'David Jimenez',
    border: 'border4'
  }
   
  ];
})


.controller('GameCtrl', function($scope, $q, $rootScope, $state) {

  $scope.matchs = [
    { id: '0'  ,
      animacion:'bounceInRight',
      imgPlayer1:'img/octavio.jpg',
      imgPlayer2:'img/david.jpg',
      name1:'Octavio Jimenez',
      name2:'David Jimenez',
      point1:6,
      point2:9,
      border1: 'border4',
      border2: 'border3',

      },
    { 
    id: '1'  ,
    animacion:'bounceInLeft',
    imgPlayer1:'img/miguel.jpg',
    imgPlayer2:'img/david.jpg',
    name1:'Octavio Jimenez',
    name2:'David Jimenez',
    point1:6,
    point2:9,
    border1: 'border1',
    border2: 'border2'
  },
  { 
      id: '2'  ,
      animacion:'bounceInRight',
      imgPlayer1:'img/octavio.jpg',
      imgPlayer2:'img/miguel.jpg',
      name1:'Octavio Jimenez',
      name2:'David Jimenez',
      point1:6,
      point2:9,
      border1: 'border1',
      border2: 'border3',


  }
   
  ];

 

  

})


.controller('MatchCtrl', function($scope) {
  $scope.players = [
    {player : "Octavio Jimenez"},
    {player : "David Jimenez"},
    {player : "Miguel"},
    
];


$scope.players2 = [
  {player2 : "Octavio Jimenez"},
  {player2 : "David Jimenez"},
  {player2 : "Miguel"}
]; 
});